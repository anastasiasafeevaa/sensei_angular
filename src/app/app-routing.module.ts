import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { FirstPage } from './first_page/first_page.component';
import { SecondPage } from './second_page/second_page.component';
import { ThirdPage } from './third_page/third_page.component';

const routes: Routes = [
  { path: 'first-page', component: FirstPage },
  { path: 'second-page', component: SecondPage },
  { path: 'third-page', component: ThirdPage },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
