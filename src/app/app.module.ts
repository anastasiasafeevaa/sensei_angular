import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FirstPage } from './first_page/first_page.component';
import { SecondPage } from './second_page/second_page.component';
import { ThirdPage } from './third_page/third_page.component'; 

@NgModule({
  declarations: [
    AppComponent,
    FirstPage,
    SecondPage,
    ThirdPage
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
