import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class DataService {

  private firstMessageSource = new BehaviorSubject('');
  firstCurrentMessage = this.firstMessageSource.asObservable();
  private secondMessageSource = new BehaviorSubject('');
  secondCurrentMessage = this.secondMessageSource.asObservable();
  private thirdMessageSource = new BehaviorSubject('');
  thirdCurrentMessage = this.thirdMessageSource.asObservable();

  constructor() { }

  firstChangeMessage(message: string) {
    this.firstMessageSource.next(message)
  }
  secondChangeMessage(message: string) {
    this.secondMessageSource.next(message)
  }
  thirdChangeMessage(message: string) {
    this.thirdMessageSource.next(message)
  }

}