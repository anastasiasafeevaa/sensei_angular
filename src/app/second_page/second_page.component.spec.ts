import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SecondPage } from './second_page.component';

describe('SecondPage', () => {
  let component: SecondPage;
  let fixture: ComponentFixture<SecondPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SecondPage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SecondPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
