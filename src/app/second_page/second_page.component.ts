import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { DataService } from '../services/sharedDataService';

@Component({
  selector: 'app-second_page',
  templateUrl: './second_page.component.html',
  styleUrls: ['./second_page.component.css']
})
export class SecondPage implements OnInit, OnDestroy {
  firstMessage: string = '';
  secondMessage: string = '';
  thirdMessage: string = '';
  firstSubscription: Subscription = new Subscription;
  secondSubscription: Subscription = new Subscription;
  thirdSubscription: Subscription = new Subscription;

  get sum() {
    return Number(this.firstMessage) + Number(this.secondMessage) - Number(this.thirdMessage);
  }
  constructor(private data: DataService) { }

  ngOnInit() {
    this.firstSubscription = this.data.firstCurrentMessage.subscribe(message => this.firstMessage = message)
    this.secondSubscription = this.data.secondCurrentMessage.subscribe(message => this.secondMessage = message)
    this.thirdSubscription = this.data.thirdCurrentMessage.subscribe(message => this.thirdMessage = message)
  }

  ngOnDestroy() {
    this.firstSubscription.unsubscribe();
    this.secondSubscription.unsubscribe();
    this.thirdSubscription.unsubscribe();

  }
}
