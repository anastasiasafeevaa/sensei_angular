import { Component, EventEmitter, OnInit, Output} from '@angular/core';
import { DataService } from '../services/sharedDataService';

@Component({
  selector: 'app-test',
  templateUrl: './first_page.component.html',
  styleUrls: ['./first_page.component.css']
})

export class FirstPage implements OnInit {
  constructor(private data: DataService) { }

  ngOnInit() {
  }

  ngOnDestroy() {
  }

  changeFirstMessage(event: any) {
    this.data.firstChangeMessage(event.target.value)
  }

  changeSecondMessage(event: any) {
    this.data.secondChangeMessage(event.target.value)
  }
}
