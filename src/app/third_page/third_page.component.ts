import { Component, EventEmitter, OnInit, Output} from '@angular/core';
import { DataService } from '../services/sharedDataService';

@Component({
  selector: 'app-third_page',
  templateUrl: './third_page.component.html',
  styleUrls: ['./third_page.component.css']
})
export class ThirdPage implements OnInit {

  constructor(private data: DataService) { }

  ngOnInit() {
  }

  ngOnDestroy() {
  }

  changeThirdMessage(event: any) {
    this.data.thirdChangeMessage(event.target.value)
  }

}
