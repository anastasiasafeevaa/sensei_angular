import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThirdPage } from './third_page.component';

describe('ThirdPage', () => {
  let component: ThirdPage;
  let fixture: ComponentFixture<ThirdPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThirdPage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThirdPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
